package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlertsPage extends BasePage{
	//Constructor, name should match classname
	//base page contains declaration of our driver and support pages
	//in alerts page constructor we have to pass webdriver. otherwise nothing will work, this is specific to tihs example.
	
	public AlertsPage (WebDriver driver) {
		super(driver); //super means whatever you pass into this, you are getting it from parent class basepage.
		
	}
	
	//page variables
	String url="https://www.training-support.net/selenium/javascript-alerts";
	//we can also have data providers, credentials etc
	
	
	
	//page elements
	public By simpleAlertButton = By.cssSelector("button#simple");
	public By confirmAlertButton = By.cssSelector("button#confirm");
	public By promptAlertButton = By.cssSelector("button#prompt");
	
	
	//page methods
	public AlertsPage goToPage()
	{
		driver.get(this.url);
		return this; //returns entire page alertspage
	}
	
	public WebElement getButton(By element) {
		WebElement button=driver.findElement(element);
		return button;
	}
	
	public void getAlertTextAndAccept() {
		
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.accept();
	}
		
	}
