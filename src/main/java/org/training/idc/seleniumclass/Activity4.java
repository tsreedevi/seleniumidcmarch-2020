package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4 {
	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver= new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/target-practice");
		
		//find grey button with absolute xPath
		driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div/div/div/div[2]/div[3]/button[2]"));
		
			
		//get third header on the page
		//;driver.findElement(By.cssSelector("#third-header"));
		WebElement w = driver.findElement(By.id("third-header"));
		System.out.println(w.getText());
		
		//get third header 
		WebElement t = driver.findElement(By.id("third-header"));
		System.out.println(t.getText());
		
		
		//get fifth header on the page and get its color CSS property
		WebElement h = driver.findElement(By.tagName("h5"));
		System.out.println(h.getCssValue("color"));
		
		//get violet button and print  the class attribute
		WebElement v = driver.findElement(By.cssSelector(".violet"));
		System.out.println(v.getAttribute("class"));
		
		
		
		driver.close();
	}

}
