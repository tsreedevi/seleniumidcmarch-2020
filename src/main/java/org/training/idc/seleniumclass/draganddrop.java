package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class draganddrop {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver= new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,20);
		driver.manage().window().maximize();
		
		driver.get("https://www.training-support.net/selenium/drag-drop");
		String title = driver.getTitle();
		System.out.println(title);
			
		
		WebElement ball=driver.findElement(By.xpath("//img[@id=\"draggable\"]"));
		
		WebElement drop1=driver.findElement(By.xpath("//div[@id=\"droppable\"]"));
		
		WebElement drop2=driver.findElement(By.xpath("//div[@id=\"dropzone2\"]"));

		Actions act=new Actions(driver);
		act.dragAndDrop(ball, drop1).build().perform();
		
		if(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/p")).isDisplayed())
			System.out.println("Droped in box1");
 
					
		Thread.sleep(1000);
		act.dragAndDrop(ball, drop2).build().perform();
		
		Thread.sleep(1000);
		if(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/p")).isDisplayed())
			System.out.println("Droped in box2");

	}

}
