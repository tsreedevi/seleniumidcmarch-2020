package org.training.idc.seleniumclass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity1 {

	public static void main(String[] args) {
		//Step 1- instantiate the Driver
		WebDriver driver = new FirefoxDriver();
		
		//Step 2 - Open Google website
		driver.get("http://google.com");
		
		//Step 3- close the browser
		driver.close();

	}

}
