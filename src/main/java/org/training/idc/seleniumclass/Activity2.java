package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) throws InterruptedException {
		//Step 1- instantiate the Driver
				WebDriver driver = new FirefoxDriver();
				
				//Step 2 - Open Google website
				driver.get("https://www.training-support.net");
				
				//
				String title = driver.getTitle();
				System.out.println("Title: "+title);
				
				driver.findElement(By.tagName("a")).click();
				Thread.sleep(10000);
				
				//Step 3- close the browser
				driver.close();

	}

}
