package org.training.idc.seleniumclass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class getrowandcolcount {

	public static void main(String[] args) {
		
		/*for table, it has tablehtml tag. it has thead which is the header and tbody which has the rows and oclumns. no of tr's inside tbody will give you the no of rows of the table.
		 * total count of tr's inside tobdy gives no of rows in table. to find no of columns, open first tr and colunt the no of td's. no of td's in the first tr of tbody gives total no of columns in the table.
		 * 
		 */
		
		WebDriver driver=new FirefoxDriver();
		driver.get("https://training-support.net/selenium/tables");
		
		//print row count
		WebElement parenttable= driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody"));
		List<WebElement> tablerows=parenttable.findElements(By.tagName("tr"));
		System.out.println("table rows:"+tablerows.size());
		
			
		//column count
		List<WebElement> cols=driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[1]/td"));
		System.out.println("table cols:"+ cols.size());
		
		//print thrid row content
		/*List<WebElement> thirdrowtext=driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[3]"));
		System.out.println("table cols:"+ thirdrowtext.toString().ge);*/
		
		WebElement thirdrow=driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[3]"));
		System.out.println("3rd row content:"+ thirdrow.getText());
		
		//get rows
		List<WebElement> rows=driver.findElements(By.xpath("//table[[contains(@class, 'striped')]/tbody/tr"));
		
		String thirdrow1=rows.get(2).getText();
		System.out.println("thirdrow1");
		
		//third row
		
		
		//cell value of each row
	/*	for (WebElement cellValue:thirdrow) {
			System.out.println(cellValue.getText());
		}*/
		
		driver.close();
		
		
	}

}
