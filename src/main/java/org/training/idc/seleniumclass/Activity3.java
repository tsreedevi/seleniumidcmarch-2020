package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://training-support.net/selenium/simple-form");
		
		String title = driver.getTitle();
		System.out.println("Page title: "+title);
		
		driver.findElement(By.id("firstName")).sendKeys("Sreedevi");
		
		driver.findElement(By.id("lastName")).sendKeys("tatineni");
		
		
		driver.findElement(By.id("email")).sendKeys("sreedevi.tatineni@gmail.com");
		driver.findElement(By.id("number")).sendKeys("1234567890");
		driver.findElement(By.tagName("textarea")).sendKeys("hello");
		
		driver.findElement(By.cssSelector("input.green")).click();
		Thread.sleep(10000);
		driver.close();
		
		
		//input.green
		
		///html/body/div[2]/div/div/div/div[2]/form/div/div[6]/div[1]/input

	}

}
