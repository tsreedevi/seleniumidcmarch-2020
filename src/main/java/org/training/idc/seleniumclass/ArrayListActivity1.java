package org.training.idc.seleniumclass;

import java.awt.List;
import java.util.ArrayList;

public class ArrayListActivity1 {

	public static void main(String[] args) {
	
		ArrayList<String> myList = new ArrayList<String>();
		myList.add("ABC");
		myList.add("dffC");
		myList.add("eee");
		myList.add("ddd");
		myList.add("333");
		
		//print 3rd element
		System.out.println("third element: "+ myList.get(2));
		
		int arrayLen=myList.size();
		
	//	for(i=0; i<arrayLen-1 )
			
			for(String s:myList) {
				System.out.println(s);
			}
		
			System.out.println("Is ddd in the list?"+myList.contains("ddd"));
			System.out.println("array list size: "+myList.size());
			System.out.println("removed:"+myList.remove(4));
			
			
			//another way
			/*
			 * Iterator<String> iter= myList.iterator();
			 * while(iter.hasNext()) { System.out.println(iter.next());}
			 */

	}

}
