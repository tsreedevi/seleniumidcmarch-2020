package org.training.idc.seleniumclass;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class alertsandpopups {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver= new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,20);
		driver.manage().window().maximize();
		
		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		String title = driver.getTitle();
		System.out.println(title);
		
		driver.findElement(By.cssSelector("button#simple")).click();
		wait.until(ExpectedConditions.alertIsPresent());
		
		Alert simpleAlert=driver.switchTo().alert();
		
		//get text from alert box and print
		String alertText=simpleAlert.getText();
		System.out.println(alertText);
		
		simpleAlert.accept();
		
		driver.close();
		
		
		
	}
}
