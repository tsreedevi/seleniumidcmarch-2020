package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class wait3 {

	public static void main(String[] args) {
		
		WebDriver driver= new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,20);
		
		driver.get("https://www.training-support.net/selenium/dynamic-attributes");
		String title = driver.getTitle();
		System.out.println(title);
		
		WebElement username=driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[1]/div/div/div[1]/input"));
		WebElement password=driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[1]/div/div/div[2]/input"));
		WebElement login=driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[1]/div/div/button"));
		
		username.sendKeys("admin");
		password.sendKeys("password");
		login.click();
		
		boolean loginsuccess=wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("action-confirmation"),"Welcome Back, admin"));
		System.out.println("login success: "+loginsuccess);
		//driver.close();
	}
}