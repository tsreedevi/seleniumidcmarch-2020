package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class wait2 {

	public static void main(String[] args) {
		
		WebDriver driver= new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,20);
		
		driver.get("https://www.training-support.net/selenium/ajax");
		String title = driver.getTitle();
		System.out.println(title);
		
		WebElement contentbutton = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/button"));
		contentbutton.click();
		boolean textwait = wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content"),"HELLO!"));
				
				//.xpath("/html/body/div[2]/div/div[2]/h1")));
				
		System.out.println("hello text exists: "+textwait);
		driver.close();
		
}
}