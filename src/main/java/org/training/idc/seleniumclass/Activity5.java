package org.training.idc.seleniumclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5 {
	
	public static void main(String[] args) throws InterruptedException {
	
	WebDriver driver = new FirefoxDriver();
	driver.get("https://training-support.net/selenium/dynamic-controls");
	
	System.out.println(driver.getTitle());
	
	WebElement findCheckbox = driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input"));
	Boolean check = findCheckbox.isSelected();
	
	if (check==true) {
	
	System.out.println("Checkbox is selected.");
	
	}else {
	
		System.out.println("Checkbox is not selected.");
	
	}
	
	findCheckbox.click();
	check = findCheckbox.isSelected();
	
	if (check==true) {
		
		System.out.println("Checkbox is selected.");
		
		}else {
		
			System.out.println("Checkbox is not selected.");
		
	}
	
	
	
	driver.close();
	
	}

}
