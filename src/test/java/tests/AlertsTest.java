package tests;

import org.testng.annotations.Test;
import pages.AlertsPage;

public class AlertsTest extends BaseTest {

	@Test
	public void simpleAlertTest()
	{
		AlertsPage page=new AlertsPage(driver);
		page.goToPage().getButton(page.simpleAlertButton).click();
		page.getAlertTextAndAccept();
		
	}
	

}
