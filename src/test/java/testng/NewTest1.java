package testng;

import org.testng.annotations.Test;

public class NewTest1 {
  @Test (priority = 2)
  public void f1() {
	  System.out.println("inside f1");
  }
  public void f2() {
	  System.out.println("inside f2. this wont print because there is no @Test annotation");
  }
  @Test (priority =1)
  public void f3() {
	  System.out.println("inside f3");
  }
  @Test 
  public void f4() {
	  System.out.println("inside f4");
  }
}
