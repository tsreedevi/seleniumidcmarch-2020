package testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameters;


public class activity12_4a {
	WebDriver driver;
	Wait wait;
	
	@BeforeTest
		public void setup() {
		driver = new FirefoxDriver();
		wait=new WebDriverWait(driver, 10);
		driver.get("https://training-support.net/selenium/login-form");
		
	}

	
	@Test
	@Parameters({"userParam","passParam"})
	public void login(String userParam, String passParam) {
	System.out.println("testing parameters");
	}
	
	
}
